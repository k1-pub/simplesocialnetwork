using System.Linq;
using Domain;
using Moq;
using Xunit;

namespace SimpleSocialNetwork.tests
{
    public class LibTests
    {
        [Fact]
        public void CanPublishPost()
        {
            var clock = new Mock<IClock>();
            var wall = new Wall(clock.Object);

            var commandFactory = new Mock<ICommandFactory>();
            commandFactory
                .Setup(c => c.Create("Jack->new message!!"))
                .Returns(new PostCommand(wall, "Jack", "new message!!"));

            var client = new Client(commandFactory.Object, wall);

            // invoke action
            client.Post("Jack->new message!!");

            // assert
            Assert.Single(wall.Posts);
            Assert.Equal("new message!!", wall.Posts.Single().Message);
            Assert.Equal("Jack", wall.Posts.Single().User);
        }
    }
}
