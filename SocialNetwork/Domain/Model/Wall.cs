﻿using System.Collections.Generic;

namespace Domain
{
    public class Wall: IWall
    {
        private readonly IClock clock;

        public Wall(IClock clock)
        {
            Posts = new List<Post>();
            this.clock = clock;
        }

        public List<Post> Posts { get; set; }

        public void PublishPost(string user, string message)
        {
            Posts.Add(new Post { User = user, Message = message, Created = clock.Now() });
        }
    }
}