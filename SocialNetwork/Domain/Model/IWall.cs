﻿namespace Domain
{
    public interface IWall
    {
        void PublishPost(string user, string message);
    }
}