﻿using System;

namespace Domain
{
    public interface IClock
    {
        DateTime Now();
    }
}