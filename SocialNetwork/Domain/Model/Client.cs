﻿namespace Domain
{
    /// <summary>
    /// Act as Invoker
    /// </summary>
    public class Client
    {
        private readonly ICommandFactory commandFactory;
        private readonly Wall wall;

        public Client(ICommandFactory commandFactory, Wall wall)
        {
            this.commandFactory = commandFactory;
            this.wall = wall;
        }

        public void Post(string v)
        {
            var cmd = commandFactory.Create(v);

            cmd.Execute();
        }
    }
}