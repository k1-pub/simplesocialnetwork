﻿
using System;

namespace Domain
{
    public class Post
    {
        public string Message { get; set; }
        public string User { get; set; }
        public DateTime Created{ get; set; }
    }
}