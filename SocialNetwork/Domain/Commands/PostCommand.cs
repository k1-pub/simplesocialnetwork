﻿namespace Domain
{
    public class PostCommand : Command
    {
        private readonly IWall wall;
        private readonly string user;
        private readonly string message;

        public PostCommand(IWall wall, string user, string message) : base(wall)
        {
            this.wall = wall;
            this.user = user;
            this.message = message;
        }

        public override void Execute()
        {
            wall.PublishPost(user, message);
        }
    }
}