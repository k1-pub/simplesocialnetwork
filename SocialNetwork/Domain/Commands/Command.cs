﻿namespace Domain
{
    public abstract class Command
    {
        private readonly IWall wall;

        public Command(IWall wall)
        {
            this.wall = wall;
        }

        public abstract void Execute();
    }
}