﻿namespace Domain
{
    public interface ICommandFactory
    {
        Command Create(string v);
    }
}